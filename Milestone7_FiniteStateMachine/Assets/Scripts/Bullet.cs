﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);

		if (col.gameObject.CompareTag("Player"))
        {
			col.gameObject.GetComponent<PlayerAI>().takeDamage(10);

			if (col.gameObject.GetComponent<PlayerAI>().getHP() <= 0)
            {
				Destroy(col.gameObject);
            }
		}

		else if (col.gameObject.CompareTag("Enemy"))
        {
			col.gameObject.GetComponent<TankAI>().takeDamage(10);

			if (col.gameObject.GetComponent<TankAI>().getHP() <= 0)
            {
				Destroy(col.gameObject);
            }
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
